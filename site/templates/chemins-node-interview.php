<?php namespace ProcessWire; 

include('_fragments.php');

if(!$config->ajax){
    echo '<div id="content">';
}

$edgesList = getEdgesLinks($page, ['chemins-node-excerpt']);
$fragmentList = getFragmentsLinks($page);
?>

<div class="screen"  data-sound="<?= ($page->sound_file)?$page->sound_file->url:'null' ?>" data-url ="<?= $page->url ?>" data-template="<?= $page->template ?>" data-id="<?= $page->id ?>" data-map-x="<?= $page->chemins_x ?>" data-map-y="<?= $page->chemins_y ?>">
        
        <div class="screen-content">
        <main>
            <article>
                <h2><?= $page->title ?></h2>
                <div class="player">
                    <div class="player-btn">
                        ⏯
                    </div>
                    <div class="progress-bar-container player-seekable">
                        <div class="progress-bar full"></div>
                    </div>
                </div>
<?php
                if($page->images->count() > 0):
?>
                <section class="splide galery">
                    <div class="splide__track">
		                <ul class="splide__list">
<?php
                    foreach($page->images as $image):
?>
                        <li class="splide__slide"><img src="<?= $image->size(1000, 0)->url ?>"></li>
<?php   
                    endforeach;
?>                     </ul>
                    </div>
                </section>
<?php
                endif;
?>

                <?= $page->text ?>
                <h3>Linked fragments</h3>
                <?= $fragmentList ?>
            </article>
<?php 
            if($page->files->count() > 0):
?>
            <aside>
                <ul>
<?php
                    foreach($page->files as $file){
                        echo '<li><a href="'.$file->url.'">'.$file->description.'</a></li>';
                    }
?>
                </ul>

            </aside>
<?php
    endif;
?>
        </main>
    </div>
    <?= $edgesList ?>
</div>

<?php 
if(!$config->ajax){
    echo '</div>';
}else{
    return $this->halt();
}
?>