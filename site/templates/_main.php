<?php namespace ProcessWire;

// Optional main output file, called after rendering page’s template file. 
// This is defined by $config->appendTemplateFile in /site/config.php, and
// is typically used to define and output markup common among most pages.
// 	
// When the Markup Regions feature is used, template files can prepend, append,
// replace or delete any element defined here that has an "id" attribute. 
// https://processwire.com/docs/front-end/output/markup-regions/
	
/** @var Page $page */
/** @var Pages $pages */
/** @var Config $config */
	
$home = $pages->get('/nodes/pages/home/'); /** @var HomePage $home */
$splash = $pages->get('/keep-informed/');


?><!DOCTYPE html>
<html lang="en">
	<head id="html-head">
		<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, shrink-to-fit=no">
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<title>SIMBA MBILI | <?php echo $page->title; ?></title>
		<link rel="stylesheet" type="text/css" href="<?php echo $config->urls->templates; ?>styles/splide.min.css" />
		
		<link rel="stylesheet" type="text/css" href="<?php echo $config->urls->templates; ?>styles/main.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo $config->urls->templates; ?>styles/responsive.css" />
		
		<script src="<?php echo $config->urls->templates; ?>scripts/smoothscroll.js"></script>
		<script src="<?php echo $config->urls->templates; ?>scripts/howler.min.js"></script>
		<script defer type="module" src="<?php echo $config->urls->templates; ?>scripts/main.js"></script>
	</head>
	<body id="html-body">
		<header>
			<div class="main-bar">
				<h2><a class="home-link" href="<?= $home->url; ?>">simba mbili</a></h2>
				<a class="splash-link" href="#"><?= $splash->title ?></a>
			</div>
		</header>
		<div id="content">
		</div>
		<div id="splash">
			<div id="close-splash">✖</div>
			<article>
				<?= $splash->text ?>
				
				<div id="register-form">
					<input id="register" type="email" placeholder="email address">
					<button id="register-button">send</button>
				</div>
			</article>
			
		</div>
		<footer>

		</footer>
	
	</body>
</html>