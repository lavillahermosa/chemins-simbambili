<?php namespace ProcessWire;

// Optional initialization file, called before rendering any template file.
// This is defined by $config->prependTemplateFile in /site/config.php.
// Use this to define shared variables, functions, classes, includes, etc. 

if($config->ajax){
    $content = trim(file_get_contents("php://input"));
    $request = json_decode($content, true);
    if(isset($request['email'])){
      
    
      $email = $sanitizer->email($request['email']);
      if($email == ''){
        echo '-1';
        exit();
      }
      
      if($pages->get('title='.$email)->id != 0){
        echo '-1';
        exit();
      }
    
      $page = wire('pages')->newPage([
        'parent' => 1504,
        'template' => 'subscriber',
        'title' => $email

      ]);
      $page->save();
      
      
      echo '1';

      exit();
    }
  }
  