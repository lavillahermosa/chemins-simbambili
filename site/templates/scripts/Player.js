const Player = ($screen) => {

    let sound = null;
    let $progressBar, $btn, $seekable;

    const load = (soundURL) => {
        if(sound != null){
            sound.unload();
        }
        sound = new Howl({
            src: [soundURL],
            onplay: () => {
                requestAnimationFrame(step);
            },
            onend: () => {
                $progressBar.style.width = '100%';
            }
        });

        
    }
    const step = () => {
        
        
        if (sound.playing()) {
            const seek = sound.seek() || 0;
   
            $progressBar.style.width = (((seek / sound.duration()) * 100) || 0) + '%';
            requestAnimationFrame(step);
        }
    };

    const play = () => {
        sound.play();
    }
    const pause = () => {
        sound.pause();
    }

    const init = () => {
        $progressBar = $screen.querySelector('.progress-bar');
        $btn = $screen.querySelector('.player-btn');
        $seekable = $screen.querySelector('.player-seekable');
        if($btn != null){
            $btn.addEventListener('click', e => {
                if(sound != null && sound.playing())
                    pause();
                else
                    play();
            });
        }
        if($seekable != null){
            $seekable.addEventListener('click', e => {
                const bounds = $seekable.getBoundingClientRect();
                const pos = ((e.clientX - bounds.left)/bounds.width);
      
                if(sound != null){
                    sound.seek(pos*sound.duration());
                    $progressBar.style.width = `${pos * 100}%`;
                }

            });
        }
        
    }   

    init();


    return {
        load, play, pause
    };
};

export default Player;