const Router = (() => {

    const getScreen = (url) => {
        return request(url);
    };

    const register = async(email) => {
      return request(window.location, {'email':email});
      
    }

    const request = async(requestUrl, postData = {}) => {
        
        const response = await fetch(requestUrl, {
          method: 'POST', // *GET, POST, PUT, DELETE, etc.
          mode: 'cors', // no-cors, *cors, same-origin
          cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
          credentials: 'same-origin', // include, *same-origin, omit
          headers: {
            'X-Requested-With': 'XMLHttpRequest',
            'Content-Type': 'application/json'
            // 'Content-Type': 'application/x-www-form-urlencoded',
          },
          redirect: 'follow', // manual, *follow, error
          referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
          body: JSON.stringify(postData) // body data type must match "Content-Type" header*/
        });

        return response;
    
    }

    return {getScreen, register};
})();

export default Router;