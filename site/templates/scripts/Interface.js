import router from "./Router.js";
import Screen from "./Screen.js";



const Interface = (() => {
    const screens = {};
    const $scene = document.getElementById('content');
    let currentScreen = null;
    let vw = window.innerWidth;
    let vh = window.innerHeight;
    const players = {};

    const init = () => {
        //at first display: first screen is already there
        const $screen = $scene.querySelector('.screen');
        currentScreen = Screen($screen);
        currentScreen.soundInit();
        screens[currentScreen.url] = currentScreen;
        currentScreen.position = {'x':vw/2, 'y':vh/2};
        
        window.addEventListener('popstate', e => {
            e.preventDefault();

            goToScreen(document.location.pathname);
        });

        const $homeLink = document.querySelector('.home-link');
        $homeLink.addEventListener('click', e => {
            e.preventDefault();
            if(currentScreen.url != $homeLink.getAttribute('href')){
                goToScreen($homeLink.getAttribute('href'), true);
            }
        });

    };

    const getAngleBetweenScreens = (screenA, screenB) => {
        return Math.atan2(screenA.mapY-screenB.mapY, screenA.mapX-screenB.mapX);
    };

    const screenCollision = (screenA, x, y) => {
        return Object.values(screens).some(screenB => 
            (   screenA != screenB && 
                x-vw/2 < screenB.x + vw/2 &&
                x + vw/2 > screenB.x - vw/2 &&
                y-vh/2 < screenB.y + vh/2 &&
                y+vh/2 > screenB.y - vh/2
            )
        );
        
    };

    const moveAllScreensBy = (x = 0, y = 0) => {
        Object.values(screens).forEach(screen => {
            screen.position = {'x':screen.x + x, 'y': screen.y + y};
        });
    };

    const computeScreenPosition = (newScreen) => {
        //get the angle between the current and the new screen
        const angle = getAngleBetweenScreens(newScreen, currentScreen);
        let positionFound = false;
        let x, y, distance = 0;
        while(!positionFound){
            distance += (vw > vh)?vw/2:vh/2;

            x = currentScreen.x + Math.cos(angle) * distance;
            y = currentScreen.y + Math.sin(angle) * distance;
            
            if(!screenCollision(newScreen, x, y)){
                positionFound = true;
            }
        }
        if(x - vw/2 < 0){
            moveAllScreensBy(-(x-vw/2), 0);
            x = vw/2;
        }
        if(y - vh/2 < 0){
            moveAllScreensBy(0, -(y-vh/2));
            y = vh/2;
        }

        
        return {'x':x, 'y':y};

    }
    const scrollToCurrentScreen = () => {
        return new Promise((resolve, reject) => {
            $scene.classList.add('moving');
            $scene.style.left = `${- (currentScreen.x - vw/2)}px`;
            $scene.style.top = `${- (currentScreen.y - vh/2)}px`;
            //smoothScroll({xPos: currentScreen.x - vw /2, yPos: currentScreen.y - vh / 2, easing:'easeOutSine', duration: 750});
            setTimeout(e => {
                $scene.classList.remove('moving');
                resolve(true);
            }, 1000);
        });
        
    }
    const goToScreen = async(url, viaLink = false) => {
        pauseAllPlayers();
        
        //first check if the screen is already there
        if(url in screens){
            currentScreen = screens[url];
            await scrollToCurrentScreen();
            currentScreen.soundInit();
            if(viaLink){
                window.history.pushState({},"", url);
            }
            return;
        }
        
        window.history.pushState({},"", url);
        
        //get the html from the router
        const response = await router.getScreen(url);
        const screenHTML = await response.text();

        const template = document.createElement('template');
        template.innerHTML = screenHTML;
        const $screen = template.content.firstChild;
        
        $scene.append($screen);
        const newScreen = Screen($screen);


        //compute the position of the new screen
        newScreen.position = computeScreenPosition(newScreen);
        $scene.style.left = `${- (currentScreen.x - vw/2)}px`;
        $scene.style.top = `${- (currentScreen.y - vh/2)}px`;
        //window.scrollTo(currentScreen.x - vw /2, currentScreen.y - vh/2);

        screens[newScreen.url] = newScreen;

        currentScreen = newScreen;

        setTimeout(async e => {
            await scrollToCurrentScreen();
            currentScreen.soundInit();
            
        }, 100);

    };

    const pauseAllPlayers = () => {
        Object.values(players).forEach(player => {
            player.pause();
        });
    };
   

    return {
        get $scene(){return $scene},
        get vw(){return vw},
        get vh(){return vh},
        get players(){return players},
        pauseAllPlayers,
        init, goToScreen};
})();

export default Interface;




