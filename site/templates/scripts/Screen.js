import Splide from "./splide.esm.js";
import Player from "./Player.js";
import itf from "./Interface.js";

const Screen = ($screen) => {

    const type = $screen.getAttribute('data-template');
    const url = $screen.getAttribute('data-url');
    const mapPosition = {'x':Number($screen.getAttribute('data-map-x')), 'y':Number($screen.getAttribute('data-map-y'))};
    const position = {'x':0, 'y':0};
    let player = null;
    let height = 0;
    


    const initEvents = () => {
        
        $screen.querySelectorAll('.to-screen').forEach($link => {
            $link.addEventListener('click', e => {
                e.preventDefault();
                itf.goToScreen($link.getAttribute('href'), true);
            });
        })
    };

    const initDisplay = () => {
        const $galeries = $screen.querySelectorAll('.splide');
        $galeries.forEach($galery => {
            var splide = new Splide($galery, {
                arrows:false
            });
            splide.mount();
        });
        height = $screen.clientHeight;
        
    };

    const soundInit = () => {
        const soundURL = $screen.getAttribute('data-sound');
        if(soundURL == null || soundURL == 'null') return;
        if(player == null){
            player = Player($screen);
            player.load(soundURL);
            itf.players[url] = player;
        }
        player.play();
        
        
    };

    initDisplay();
    initEvents();


    return {
        set position(value){
            position.x = value.x;
            position.y = value.y;
            $screen.style.left = `${position.x - itf.vw / 2}px`;
            $screen.style.top =  `${position.y - itf.vh / 2}px`;
        },
        get x(){
            return position.x;
        },
        get y(){
            return position.y;
        },
        get height(){
            return height;
        },
        get $screen(){
            return $screen;
        },
        get url(){
            return url;
        },
        get mapX(){return mapPosition.x},
        get mapY(){return mapPosition.y},
        soundInit
    }; 
};

export default Screen;