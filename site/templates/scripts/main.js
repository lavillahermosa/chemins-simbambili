import player from "./Player.js";
import itf from "./Interface.js";
import router from "./Router.js";

setTimeout(e => itf.init(), 1000);


const $splash = document.getElementById('splash');
const $splashLink = document.querySelector('.splash-link');
const $registerForm = document.getElementById('register-form');
const $registerField = document.getElementById('register');
const $registerButton = document.getElementById('register-button');

$splash.addEventListener('click', e => {
    console.log(e.target);
    if(e.target != $registerField && e.target != $registerButton)
        $splash.classList.add('hidden');
});

$splashLink.addEventListener('click', e => {
    $splash.classList.remove('hidden');
    $registerForm.classList.remove('submitted');
});

$registerButton.addEventListener('click', async(e) => {
      
    const response = await router.register($registerField.value);
    const result = await response.text();

    if(parseInt(result) === 1){
        $registerForm.classList.add('submitted');
    }

});