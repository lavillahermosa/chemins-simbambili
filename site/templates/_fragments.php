<?php namespace ProcessWire; 


function getEdgesLinks($page, $ignore = []){
    
    $linkedEdges = $page->references();
    $linkedNodes = array(
        'chemins-node-page' => new PageArray(),
        'chemins-node-topic' => new PageArray(), 
        'chemins-node-interview' => new PageArray(), 
        'chemins-node-excerpt' => new PageArray()
    );

    foreach($linkedEdges as $linkedEdge){
        $linkedNode = ($linkedEdge->chemins_node_a == $page)?
            $linkedEdge->chemins_node_b:$linkedEdge->chemins_node_a;
        

        $linkedNode->displayTitle = ($linkedEdge->title != '')?$linkedEdge->title:$linkedNode->title;
        if(!in_array($linkedNode->template, $ignore)){
            
            $linkedNodes[$linkedNode->template->name]->add($linkedNode);
            
        }
    }
    
    $output = '<ul class="edges-nav">';
    foreach($linkedNodes as $template => $nodes){
        foreach($nodes as $linkedNode){
            $angle = atan2($linkedNode->chemins_y - $page->chemins_y, $linkedNode->chemins_x - $page->chemins_x);
            
            $output .=  '<li><a class="to-screen" href="'.$linkedNode->url.'"><span><span>'.$linkedNode->displayTitle.'</span> <span style="transform:rotate('.$angle.'rad);" class="link-direction">></span></span></a>
                    </li>';
        }

    }
    

   

    $output .= '</ul>';
    
    return $output;


}

function getFragmentsLinks($page){
    $output = '<ul class="linked-fragments">';
    $linkedEdges = $page->references();
    $linkedNodes = new PageArray();
    foreach($linkedEdges as $linkedEdge){
        $linkedNode = ($linkedEdge->chemins_node_a == $page)?
            $linkedEdge->chemins_node_b:$linkedEdge->chemins_node_a;
        if($linkedNode->template == 'chemins-node-excerpt'){
            $linkedNodes->add($linkedNode);
            $angle = atan2($linkedNode->chemins_y - $page->chemins_y, $linkedNode->chemins_x - $page->chemins_x);
            $output .=  '<li><a class="to-screen" href="'.$linkedNode->url.'"><span><span>'.(($linkedEdge->title != '')?$linkedEdge->title:$linkedNode->title).'</span> <span style="transform:rotate('.$angle.'rad);" class="link-direction">></span></span></a>
                    </li>';
        }
    }

    $output .= '</ul>';
    
    return $output;
        
}