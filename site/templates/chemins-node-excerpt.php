<?php namespace ProcessWire; 

include('_fragments.php');

if(!$config->ajax){
    echo '<div id="content">';
}

$edgesList = getEdgesLinks($page);
?>

<div class="screen" data-sound="<?= ($page->sound_file)?$page->sound_file->url:'null' ?>" data-url ="<?= $page->url ?>" data-template="<?= $page->template ?>" data-id="<?= $page->id ?>" data-map-x="<?= $page->chemins_x ?>" data-map-y="<?= $page->chemins_y ?>">
    <div class="screen-content player-btn">
        
        <main> 
        <div class="progress-bar excerpt">
            <div><h1><?= $page->title ?></h1></div>
        </div>
        
        <article>
                <h1><?= $page->title ?></h1>
            

        </article>
    </div>
    <?= $edgesList ?>
</div>

<?php 
if(!$config->ajax){
    echo '</div>';
}else{
    return $this->halt();
}
?>