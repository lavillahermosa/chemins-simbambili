import router from './js/Router.js';
import itf from './js/Interface.js';
import session from './js/Session.js';



window.addEventListener('load', e=>{
    document.querySelectorAll('.loader-font-1').forEach(($loader) => {
        $loader.remove();
    });
    itf.init();
    session.controlLoop();
});