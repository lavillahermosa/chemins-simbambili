<?php 
namespace ProcessWire;
class CheminsConfig extends ModuleConfig {
    public function __construct() {
        $this->add(array(
          array(
            'name' => 'prefix',
            'label' => 'Templates and fields prefix',
            'type' => 'text',
            'required' => true,
            'value' => 'chemins',
          )
        ));
    }
}