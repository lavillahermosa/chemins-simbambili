const Router = (() => {

    const getNodeTypes = async() => {
        return request(window.location.href, {'action':'getNodeTypes'});
    };

    const getEdgeTypes = async() => {
        return request(window.location.href, {'action':'getEdgeTypes'});
    };

    const saveNode = async(title, parentId, template, x, y) => {
        return request(window.location.href, {
            'action':'saveNode', 'title':title, 
            'parentId':parentId, 'template':template, 
            'x':x, 'y':y
        });
    };
                        
    const saveEdge = async(title, parentId, template, nodeAId, nodeBId) => {
        return request(window.location.href, {
            'action':'saveEdge', 'title':title, 
            'parentId':parentId, 'template':template, 
            'nodeAId':nodeAId, 'nodeBId':nodeBId
        });
    };

    const updatePage = async(id, data) => {
        return request(window.location.href, {
            'action':'updatePage', 'id':id, 
            'data':data
        });
    };

    const getNode = (id) => {
        return request(window.location.href, {'action':'getNode', 'id':id});
    };

    const getNodes = () => {
        return request(window.location.href, {'action':'getNodes'});
    };

    const getEdge = (id) => {
        return request(window.location.href, {'action':'getEdge', 'id':id});
    };

    const getEdges = () => {
        return request(window.location.href, {'action':'getEdges'});
    };

    const removeEdge = (id) => {
        return request(window.location.href, {'action':'removeEdge', 'id':id});
    };

    const removeNode = (id) => {
        return request(window.location.href, {'action':'removeNode', 'id':id});
    };

    const upEdge = (id) => {
        return request(window.location.href, {'action':'upEdge', 'id':id});
    };

    const getControl = () => {
        return request(window.location.href, {'action':'getControl'});
    };


    const request = async(requestUrl, postData = {}) => {
        
        const response = await fetch(requestUrl, {
          method: 'POST', // *GET, POST, PUT, DELETE, etc.
          mode: 'cors', // no-cors, *cors, same-origin
          cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
          credentials: 'same-origin', // include, *same-origin, omit
          headers: {
            'X-Requested-With': 'XMLHttpRequest',
            'Content-Type': 'application/json'
            // 'Content-Type': 'application/x-www-form-urlencoded',
          },
          redirect: 'follow', // manual, *follow, error
          referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
          body: JSON.stringify(postData) // body data type must match "Content-Type" header*/
        });

        return response.json();
    
    }

    return {getNodes, getNode, getNodeTypes, getEdgeTypes, saveNode, removeNode, updatePage, getEdge, getEdges, saveEdge, removeEdge, upEdge, getControl};
})();

export default Router;