import { drawFromPoints, computeEllipse, computeLosange, computeRectangle, showPoints } from "./Draw.js";

const Node = ($container, {
    title = null, 
    x = 0, y = 0, 
    id = null, 
    nodeType = null, 
    selected = false, disabled = false
    } = {}) => {

        const snapElem = Snap();
        const $node = document.createElement('div');
        $node.classList.add('node');
        const $title = document.createElement('h3');
        const strokeWidth = 2, definition = 12, margin = (nodeType.border == 'curly')?10:0, strokeColor = '#007120', fillColor = 'transparent';
        let width, height;
        
        //shapeB is a path only used for computation (hack to avoid bug in intersection test)
        let shapePoints, shape, shapeB;
        
        

        const computeShape = () => {
            $node.style.width = 'auto';
            $node.style.height = 'auto';

            width = $title.getBoundingClientRect().width + margin * 2;
            height = $title.getBoundingClientRect().height + margin * 2;

            switch(nodeType.shape){
                case 'losange':
                    console.log('ratio', width/height);
                    if(width/height > 1.5){
                        height = width / 1.5;
                    }
            }
          

            $node.style.width = `${width}px`;
            $node.style.height = `${height}px`;

            snapElem.clear();
            snapElem.attr({
                width:width,
                height:height
            });

            shapePoints = computeShapePoints(nodeType.shape);
            
            shape = drawFromPoints(snapElem, shapePoints, nodeType.border, true);
            if(nodeType.border == 'straight'){
                const  shapePointsB = computeShapePoints(nodeType.shape, 10);
                shapeB = drawFromPoints(snapElem, shapePointsB, nodeType.border, true);

            }else{
                shapeB = shape;
            }
            shapeB.attr({
                display:'none'
            });

            shape.attr({
                display:'inline',
                fill:fillColor,
                stroke:strokeColor,
                strokeWidth:strokeWidth
            });

        };

        const init = () => {
            $title.innerText = title;
            
            $node.appendChild(snapElem.node);
            $node.appendChild($title);
            $node.setAttribute('data-id', id);
            $container.appendChild($node);
            
            //first compute shape before moving into position (allows to keep the node single line by default)
            computeShape();
            setPosition(x, y);
            
            

            //showPoints(snapElem, shapePoints);
            
            
        };
        
        const getBorderPoint = (angle) => {
            const limitPoint = {
                x:(width/2) + Math.cos(angle) * 300,
                y:(height/2) + Math.sin(angle) * 300
            };
            const intersectionPath = snapElem.path(`M${width/2}, ${height/2} L${limitPoint.x}, ${limitPoint.y}`);
            intersectionPath.attr({
                display:'none'
            });

            const intersects = Snap.path.intersection(shapeB, intersectionPath);
            if(intersects.length == 0) return false;
            //if(intersects.length > 0){
            const borderPoint = {
                'x':intersects[intersects.length - 1].x,
                'y':intersects[intersects.length - 1].y
            };
            

            return borderPoint;

        };

        

        
        const setPosition = (newX, newY) => {
            x = newX;
            y = newY;
            $node.style.left = `${x}px`;
            $node.style.top = `${y}px`;
        };


        const computeShapePoints = (shape, def = definition) => {
            switch(shape){
                case 'ellipse':
                    return computeEllipse(width/2, height/2, 
                        width - margin * 2 - strokeWidth*2, height - margin * 2 - strokeWidth*2, def);
                case 'losange':
                    return computeLosange(margin + strokeWidth, width-margin  - strokeWidth, margin + strokeWidth, height - margin - strokeWidth, def);
                case 'rectangle':
                    return computeRectangle(margin + strokeWidth, margin + strokeWidth, width - margin - strokeWidth * 2, height-margin - strokeWidth * 2);
                    
            }
            
        };
        const disable = () => {
            disabled = true;
            $node.classList.add('disabled');
        }
        const enable = () => {
            disabled = false;
            $node.classList.remove('disabled');
        }
        const select = () => {
            selected = true;
            $node.classList.add('selected');
            
        }
        const unselect = () => {
            selected = false;
            $node.classList.remove('selected');
        }

        const removeElement = () => {
            $node.remove();
        };
        
        init();

        return {
            set id(value) {
                id = value;
                $node.setAttribute('data-id', id);
            },
            set x(value){
                x = value;
                $node.style.left = `${x}px`;
            },
            set y(value){
                y = value;
                $node.style.top = `${y}px`;
            },
            set title(value){
                title = value;
                $title.innerText = title;
                computeShape();
            },
            set nodeType(value){
                nodeType = value;
                computeShape();
            },
            
            get id(){return id},
            get title(){return title},
            get nodeType(){return nodeType}, 
            get $node(){return $node},
            get x(){return x},
            get y(){return y},
            get width(){return width},
            get height(){return height},
            get margin(){return margin},
            get selected(){return selected},
            get disabled(){return disabled},

            setPosition,
            enable, disable, select, unselect,
            getBorderPoint, removeElement
        };

};

export default Node;