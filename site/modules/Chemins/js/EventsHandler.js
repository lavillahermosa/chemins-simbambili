//centralize all the events (trying to make a better global event handling that avoids conflict)
import itf from "./Interface.js";
import router from "./Router.js";
import session from "./Session.js";

const EventsHandler = (() => {
    
    let keysPressed = [];
    let inProgress = [];
    

    //Global keyboard listener = keep a list of keys being pressed 
    document.addEventListener('keydown', e => keysPressed.push(e.key));
    document.addEventListener('keyup', e => keysPressed = keysPressed.filter(key => key !== e.key));

    //INTERFACE EVENTS
    const registerItfEvents = async() => {

        const onItfScroll = () => {
            //space arrows
            itf.toggleSpaceArrows({
                top:window.scrollY == 0, 
                right:window.scrollX + window.innerWidth >= document.body.scrollWidth,
                bottom:window.scrollY + window.innerHeight >= document.body.scrollHeight,
                left:window.scrollX == 0
            });
            
        };
        
        const onSceneClick = async(e) => {

            if(inProgress.length > 0)return;
            inProgress.push(itf.tools.current);
            switch(itf.tools.current){
                case 'add-node':
                    if(!session.hasControl()) return;
                    const nodeData = await itf.nodeForm();
                    if(nodeData != null){
                        const node = itf.createNode(nodeData.title, nodeData.type, e.pageX, e.pageY);
                        const nodeId = await router.saveNode?.(nodeData.title, nodeData.type, node.nodeType.template, e.pageX, e.pageY);
                        node.id = nodeId;
                        itf.registerNode(node); 
                    }
                    break;
                
                        
            };

            inProgress = [];
        };
            

        itf.$spaceArrows.forEach($spaceArrow => $spaceArrow.addEventListener('click', e => {
            e.stopPropagation();
            if(!session.hasControl()) return;
            itf.addSpace($spaceArrow.getAttribute('data-direction'));
        }));

        document.addEventListener('scroll', onItfScroll);
        itf.$scene.addEventListener('click', onSceneClick);

        //trigger scroll for the first time
        onItfScroll();



        //tools events
        itf.tools.$toolsUI.addEventListener('mouseenter', e => itf.tools.showUI());
        itf.tools.$toolsUI.addEventListener('mouseleave', e => itf.tools.hideUI());
        itf.tools.$toolsButtons.forEach($b => {
            $b.addEventListener('mouseenter', e => itf.tools.showDesc($b.getAttribute('data-tool')));
            $b.addEventListener('mouseleave', e => itf.tools.hideDesc($b.getAttribute('data-tool')));
            $b.addEventListener('click', e => {
                e.stopPropagation();
                if(inProgress.length > 0){
                    cancelOperations();
                }
                itf.tools.switchTool($b.getAttribute('data-tool'));
            });      
        });


        
        
        
    }


    //NODE EVENTS
    const registerNodeEvents = (node) => {
        const onNodeMouseDown = (e) => {
            //MOVE NODE
            if(itf.tools.current == 'move-node'){
                e.stopPropagation();
                if(!session.hasControl()) return;
                let mousePosX = e.pageX, mousePosY = e.pageY;
                const moveNode = (e) => {
                    const deltaX = mousePosX - e.pageX;
                    const deltaY = mousePosY - e.pageY;
                    mousePosX = e.pageX;
                    mousePosY = e.pageY;
                    node.setPosition(node.x-deltaX, node.y-deltaY);
                    itf.updateLinkedEdges(node);
                    
                }
                
                node.$node.classList.add('dragging');
                document.addEventListener('mousemove', moveNode);
                document.body.addEventListener('mouseup', e => {
                    document.removeEventListener('mousemove', moveNode);
                    node.$node.classList.remove('dragging');
                    router.updatePage(node.id, {'x':node.x, 'y':node.y}); 
                }, {once:true});
            }
        };
        
        const onNodeClick = async(e) => {
            if(!session.hasControl() || node.disabled) return;
            switch(itf.tools.current){
                case 'rename':
                    
                    const nodeData = await itf.nodeForm({id:node.id, title:node.title, type:node.nodeType.parent});
                    if(nodeData == null) return;
                    if(nodeData.title != node.title || nodeData.type != node.nodeType.parent){
                        const newData = {};
                        if(nodeData.title != node.title){
                            newData.title = nodeData.title;
                            node.title = nodeData.title;
                        }
                        if(nodeData.type != node.nodeType.parent){
                            node.nodeType = itf.nodeTypes[nodeData.type];
                            newData.parent = nodeData.type;
                            newData.template = node.nodeType.template;
                        }
                        if(Object.keys(newData).length == 0)
                            return;

                        itf.updateLinkedEdges(node);
                        router.updatePage(node.id, newData);
                    }
                    break;
                case 'add-edge':
                    
                    if(!itf.selectNode(node)){
                        endProcess();
                        return;
                    };
                    
                    if(itf.selectedNodes.length == 2){
                            const edgeData = await itf.edgeForm();
                        if(edgeData != null){
                            console.log(edgeData);
                            const edge = itf.createEdge(edgeData.type, itf.selectedNodes[0], itf.selectedNodes[1], edgeData.title);
                            const edgeId = await router.saveEdge(edge.title, 
                                edgeData.type, edge.edgeType.template, edge.nodeA.id, edge.nodeB.id);
            
                            edge.id = edgeId;
                            itf.registerEdge(edge);
                            
                            
                        }
                        endProcess();
                        
                    }

                    else{
                        inProgress.push('add-edge');
                        itf.disableLinkedEdges(node);
                    }
                    break;
                case 'open-form':
                    const url = `${location.protocol}//${location.host}${location.pathname}edit/?id=${node.id}`;
                    const $form = pwModalWindow(url, {
                        close: (e, ui) => {
                            return new Promise(async(resolve, reject)  => {
                               
                                const nodeData = await router.getNode(node.id);
                                resolve(nodeData);
                            }).then(nodeData => {
                                const statuses = Object.values(nodeData.status);
                                if(statuses.includes('trash')){
                                    itf.removeNode(node);
                                    return;
                                }
                                node.title = nodeData.title;
                                node.x = nodeData.x;
                                node.y = nodeData.y;
                                itf.updateLinkedEdges(node);
                            });
                             
                          
                            
                        }
                    }, 'medium');
                     
                    break;
                
                case 'delete':
                    itf.modalForm({'title':`delete ${node.title}?`, 'focus':'validate'}).then(v => {
                       
                        const linkedEdges = itf.edgesByNodeId[node.id];
                        if(linkedEdges != undefined){
                            linkedEdges.forEach(edge => {
                                itf.removeEdge(edge);
                                router.removeEdge(edge.id);
                            });
                        }
                        itf.removeNode(node);
                        router.removeNode(node.id);
                    }).catch(r => {
                        console.log(r);
                    });
                    
                


            }
        };
        
        node.$node.addEventListener('click', onNodeClick);

        node.$node.addEventListener('mousedown', onNodeMouseDown);
    
    };

    //EDGE EVENTS
    const registerEdgeEvents = (edge) => {
        const onEdgeClick = async(e) => {
            
            switch(itf.tools.current){
                case 'rename':
                    if(!session.hasControl()) return;
                    const edgeData = await itf.edgeForm({id:edge.id, title:edge.title, type:edge.edgeType.parent});
                    if(edgeData == null) return;
                    const newData = {};
                    if(edgeData.title != edge.title){
                        newData.title = edgeData.title;
                        edge.title = edgeData.title;
                    }
                    if(edgeData.type != edge.edgeType.parent){
                        edge.edgeType = itf.edgeTypes[edgeData.type];
                        newData.parent = edgeData.type;
                        newData.template = edge.edgeType.template;
                    }
                    if(Object.keys(newData).length == 0)
                        return;

                    router.updatePage(edge.id, newData);    

                    break;
                case 'up-depth':
                    if(!session.hasControl()) return;
                    itf.upEdge(edge);
                    router.upEdge(edge.id);
                    break;
                case 'reverse-edge':
                    if(!session.hasControl()) return;
                    edge.reverse();
                    router.updatePage(edge.id, {'node_a':edge.nodeA.id, 'node_b':edge.nodeB.id});
                    break;
                case 'open-form':
                    const url = `${window.location.href}edit/?id=${edge.id}`;
                    
                    const $form = pwModalWindow(url, {
                        close: (e, ui) => {
                            return new Promise(async(resolve, reject)  => {
                                const edgeData = await router.getEdge(edge.id);
                                resolve(edgeData);
                            }).then(edgeData => {
                                const statuses = Object.values(edgeData.status);
                                if(statuses.includes('trash')){
                                    itf.removeEdge(edge);
                                    return;
                                }
                                edge.title = edgeData.title;
                                if(edge.nodeA.id != edgeData.nodeA || edge.nodeB.id != edgeData.nodeB){
                                    itf.changeLinkedNodes(edge, edgeData.nodeA, edgeData.nodeB);

                                }
                            });
                             
                          
                            
                        }
                    }, 'medium');
                    
                    break;

                case 'delete':
                    itf.modalForm({'title':`delete ${edge.title}?`, 'focus':'validate'}).then(v => {
                        itf.removeEdge(edge);
                        router.removeEdge(edge.id);
                    }).catch(r => {
                        console.log(r);
                    });
                    
                    break;
            

            }
        };

        edge.$edge.addEventListener('click', onEdgeClick);
    };


    const endProcess = () => {
        inProgress.forEach(operation => {
            switch(operation){
                case 'add-edge':
                    const unselectedNodes = itf.unselectNodes();
                    unselectedNodes.forEach(node =>  itf.enableLinkedEdges(node));
                    break;
            }
        });
        inProgress = [];
    };


    return {registerItfEvents, registerNodeEvents, registerEdgeEvents};
})();

export default EventsHandler;