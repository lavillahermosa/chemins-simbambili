import router from "./Router.js";
import itf from "./Interface.js"

const Session = (() => {

    let status = null;

    const takeControl = async() => {
        const response = await router.getControl?.();
        return response;
    }

    const controlLoop = async() => {

        const newStatus = await takeControl();
        console.log(newStatus);
        if(newStatus != status)
            statusChange(newStatus);
        setTimeout(controlLoop, 60000);

    }

    const statusChange = (newStatus) => {
        if(status == null || newStatus.control != status.control){
            if(newStatus.control == 0){
                itf.displayInfo(`Hello ${newStatus.username}. You just received the modifications rights.`, 'validation');
            }else if(newStatus.control == 1){
                itf.displayInfo(`Welcome,  ${newStatus.username}. You don't have the modifications rights but you can still view the map!`, 'validation');
            }else{
                itf.displayInfo(`Hello,  ${newStatus.username} is already working here.`, 'error');
            }
        } 
        status = newStatus;

    };

    const hasControl = () => {
        return status != null && status.control == 0?true:false;
    }

    return {controlLoop, hasControl};
})();

export default Session;