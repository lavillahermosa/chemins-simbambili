<?php
namespace ProcessWire;

function getNodes($prefix){
    $nodes = [];
    $lng = wire('languages')->getLanguage();
    $nodesContainer = wire('pages')->get('/nodes/');
    $nodePages = wire('pages')->find('parent='.$nodesContainer->children);
    foreach($nodePages as $nodePage){
        $node = ['id' => $nodePage->id, 'parent' => $nodePage->parent->id, 
            'title' => $nodePage->title->getLanguageValue($lng),
            'x' => $nodePage->{$prefix.'_x'},
            'y' => $nodePage->{$prefix.'_y'},
            'status' => $nodePage->status(true)
        ];
        $nodes[] = $node;
    }
    return $nodes;



}

function getNode($prefix, $id){
    $nodePage = wire('pages')->get($id);
    $lng = wire('languages')->getLanguage();

    $node = ['id' => $nodePage->id, 'parent' => $nodePage->parent->id, 
            'title' => $nodePage->title->getLanguageValue($lng),
            'x' => $nodePage->{$prefix.'_x'},
            'y' => $nodePage->{$prefix.'_y'},
            'status' => $nodePage->status(true)
    ];

    return $node;
}

function getEdges($prefix){
    $edges = [];
    $lng = wire('languages')->getLanguage();
    $edgesContainer = wire('pages')->get('/edges/');
    $edgePages = wire('pages')->find('parent='.$edgesContainer->children);
    foreach($edgePages as $edgePage){
        $edge = ['id' => $edgePage->id, 'parent' => $edgePage->parent->id, 
            'title' => $edgePage->title->getLanguageValue($lng),
            'nodeA' => $edgePage->{$prefix.'_node_a'}->id,
            'nodeB' => $edgePage->{$prefix.'_node_b'}->id,
            'status' => $edgePage->status(true)
        ];
        $edges[] = $edge;
    }
    return $edges;



}

function getEdge($prefix, $id){
    $edgePage = wire('pages')->get($id);
    $lng = wire('languages')->getLanguage();
    $edge = ['id' => $edgePage->id, 'parent' => $edgePage->parent->id, 
            'title' => $edgePage->title->getLanguageValue($lng),
            'nodeA' => $edgePage->{$prefix.'_node_a'}->id,
            'nodeB' => $edgePage->{$prefix.'_node_b'}->id,
            'status' => $edgePage->status(true)
        ];
    return $edge;
}

function getNodeTypes($prefix){
    $nodeTypes = [];
    $nodesContainer = wire('pages')->get('/nodes/');
    $defaultShape = wire('pages')->get('/settings/shapes/')->child();
    $defaultBorder = wire('pages')->get('/settings/strokes/')->child();
    $lng = wire('languages')->getLanguage();

    foreach($nodesContainer->children as $parentNode){

        $childTemplates = $parentNode->template->childTemplates();
        
        
        $border = ($parentNode->{$prefix.'_stroke'}->id != 0)?
            $parentNode->{$prefix.'_stroke'}->name:$defaultBorder->name;
        $shape = ($parentNode->{$prefix.'_shape'}->id != 0)?
            $parentNode->{$prefix.'_shape'}->name:$defaultShape->name;
        
        $nodeTypes[$parentNode->id] = [
            'parent' => $parentNode->id, 
            'name' => $parentNode->{$prefix.'_singular_title'}->getLanguageValue($lng),
            'template' => $childTemplates->first()->name,
            'border' => $border,
            'shape' => $shape
        ];
        
    }
    return $nodeTypes;
}
function getEdgeTypes($prefix){
    $edgeTypes = [];

    $edgesContainer = wire('pages')->get('/edges/');
    $defaultBorder = wire('pages')->get('/settings/strokes/')->child();
    $lng = wire('languages')->getLanguage();

    foreach($edgesContainer->children as $parentEdge){

        $childTemplates = $parentEdge->template->childTemplates();
        
        $border = ($parentEdge->{$prefix.'_stroke'}->id != 0)?
            $parentEdge->{$prefix.'_stroke'}->name:$defaultBorder->name;
        
        
        $edgeTypes[$parentEdge->id] = [
            'parent' => $parentEdge->id, 
            'name' => $parentEdge->{$prefix.'_singular_title'}->getLanguageValue($lng),
            'template' => $childTemplates->first()->name,
            'border' => $border
        ];
        
    }
    return $edgeTypes;
}


function saveNode($prefix, $title, $parentId, $template, $x, $y){
    $page = wire('pages')->newPage([
        'template' => $template,
        'parent' => $parentId,
        'title' => $title,
        $prefix.'_x' => $x,
        $prefix.'_y' => $y

    ]);
    $page->save();

    return $page->id;
}
function saveEdge($prefix, $title, $parentId, $template, $nodeAId, $nodeBId){
    $page = wire('pages')->newPage([
        'template' => $template,
        'parent' => $parentId,
        'title' => $title,
        $prefix.'_node_a' => $nodeAId,
        $prefix.'_node_b' => $nodeBId

    ]);
    //print_r($page);
    $page->save();

    return $page->id;
}

function pageSortLast($id){
    $page = wire('pages')->get($id);
    $lastChild = $page->parent()->children()->last();
    $page->sort = $lastChild->sort + 1;
    $page->save();
    return $lastChild->id.' '.$lastChild->sort;
}

function deletePage($id){
    $page = wire('pages')->get($id);
    $page->trash();
    return true;
}

function updatePage($prefix, $id, $data){
    $page = wire('pages')->get($id);
    foreach($data as $field => $value){
        if($page->get($prefix.'_'.$field))
            $page->set($prefix.'_'.$field, $value);
        else if($page->get($field))
            $page->set($field, $value);
    }

    $page->save();
    return true;
}




function createSession($prefix){
    $now = time();
    
    $page = wire('pages')->newPage([
        'template' => $prefix.'-session',
        'parent' => wire('pages')->get('/sessions/'),
        $prefix.'_username' => wire('user')->name,
        'title' => date('Y-m-d H:i:s', $now).' - '.wire('user')->name,
        $prefix.'_date_begin' => $now,
        $prefix.'_date_end' => $now + 2 * 60,
        $prefix.'_session_token' => uniqid()
    ]);
    $page->save();
    return $page;
}

function getSessionPage($prefix){
    $now = time();
    $currentSession = wire('pages')->get(
        'template='.$prefix.'-session,'.
        $prefix.'_date_begin<'.$now.','.
        $prefix.'_date_end>'.$now);

    return $currentSession;
  
}

function getControlStatus($prefix){
    //control 0 = ok
    //control 1 = no edit rights
    //control 2 = session is owned by somebody else

    $control = 1;
    
    if(!wire('user')->hasPermission('chemins-edit')){
        return ['control' => $control, 'username' => wire('user')->name];
    }

    $currentSession = getSessionPage($prefix);
    //print_r($currentSession);
    if($currentSession->id == 0){
        $currentSession = createSession($prefix);
        wire('session')->sessionToken = $currentSession->{$prefix.'_session_token'};
    }
    //hasAccess

    if(wire('session')->get('sessionToken') != null && 
       wire('session')->get('sessionToken') == $currentSession->{$prefix.'_session_token'}){
        $currentSession->{$prefix.'_date_end'} = time() + 2 * 60;
        $currentSession->save();
        $control = 0;
    }else{
        $control = 2;
    }

    return ['control' => $control, 'username' => $currentSession->{$prefix.'_username'}];

}